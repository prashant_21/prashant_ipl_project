const fs = require('fs');
const csv = require('csvtojson');

const matchesPath = 'src/data/matches.csv';
const deliveriesPath = 'src/data/deliveries.csv';
const ipl = require('./ipl.js');

const matchesPerYearPath = 'src/public/output/matchesPerYear.json';
const matchesWonPerTeamPath = 'src/public/output/matchesWonPerTeam.json';
const extraRunsConcededPerTeamPath = 'src/public/output/extraRunsConcededPerTeam_2016.json';
const top10EconomicalBowlersPath = 'src/public/output/top10EconomicalBowlers_2015.json';

/**
 * Number of matches played per year for all the years in IPL.
 */

function matchesPerYear() {
    csv()
        .fromFile(matchesPath)
        .then((matches) => {
            const output = ipl.matchesPerYear(matches);
            fs.writeFile(matchesPerYearPath, JSON.stringify(output), (err) => {
                console.log(err);
            }
            );
        })
        .catch((err) => {
            console.log(err)
        });
}
//End
//-----------------------------------------------------------------------------------------

/**
 * Number of matches won per team per year in IPL.
 */

function matchesWonPerTeam() {
    csv()
        .fromFile(matchesPath)
        .then((matches) => {
            const output = ipl.matchesWonPerTeam(matches);
            fs.writeFile(matchesWonPerTeamPath, JSON.stringify(output), (err) => {
                console.log(err);
            }
            );
        })
        .catch((err) => {
            console.log(err);
        });
}
//End
//----------------------------------------------------------------------------------------

/**
 * Extra runs conceded per team in the year 2016.
 */

function extraRunsConcededPerTeam_2016() {
    csv()
        .fromFile(matchesPath)
        .then((matches) => {
            csv()
                .fromFile(deliveriesPath)
                .then((deliveries) => {
                    const output = ipl.extraRunsConcededPerTeam_2016(matches, deliveries);
                    fs.writeFile(extraRunsConcededPerTeamPath, JSON.stringify(output), (err) => {
                        console.log(err);
                    });
                })
                .catch((err) => {
                    console.log(err);
                });
        })
        .catch((err) => {
            console.log(err);
        });
}
//End
//----------------------------------------------------------------------------------------

/**
 * Top 10 economical bowlers in the year 2015.
 */

function top10EconomicalBowlers_2015() {
    csv()
        .fromFile(matchesPath)
        .then((matches) => {
            csv()
                .fromFile(deliveriesPath)
                .then((deliveries) => {
                    const output = ipl.top10EconomicalBowlers_2015(matches, deliveries);
                    fs.writeFile(top10EconomicalBowlersPath, JSON.stringify(output), (err) => {
                        console.log(err);
                    });
                })
                .catch((err) => {
                    console.log(err);
                });
        })
        .catch((err) => {
            console.log(err);
        });
}
//End
//----------------------------------------------------------------------------------------

matchesPerYear();
matchesWonPerTeam();
extraRunsConcededPerTeam_2016();
top10EconomicalBowlers_2015();