/**
 * Total number of matches played per year.
 * @param {*} matches: matches.csv data in json format 
 */
function matchesPerYear(matches) {
  const numberOfMatches = {};
  matches.reduce((numberOfMatch, match) => {
    if (numberOfMatches[match['season']] === undefined) {
      numberOfMatches[match['season']] = 1;
    }
    else {
      numberOfMatches[match['season']]++;
    }
  });
  return numberOfMatches;
}

//End
//------------------------------------------------------------------------------------

/**
 * Matches won per team per year.
 * @param {*} matches: matches.csv data in json format 
 */

function matchesWonPerTeam(matches) {
  const numberOfWinnings = {};
  matches.reduce((numberOfWinning, match) => {
    if (numberOfWinnings[match['season']] === undefined) {
      numberOfWinnings[match['season']] = {};
      numberOfWinnings[match['season']][match['winner']] = 1;
    }
    else {
      if (numberOfWinnings[match['season']][match['winner']] === undefined) {
        numberOfWinnings[match['season']][match['winner']] = 1;
      }
      else {
        numberOfWinnings[match['season']][match['winner']]++;
      }
    }
  });
  return numberOfWinnings;
}

//End
//-----------------------------------------------------------------------------------

/**
 * Extra runs conceded per team in the year 2016.
 * @param {*} matches: matches.csv data in json format  
 * @param {*} deliveries: deliveries.csv data in json format  
 */

function extraRunsConcededPerTeam_2016(matches, deliveries) {
  const extraRuns = {};
  let matchId_2016 = matches.filter((match) => match.season == 2016).map(
    (map) => map.id
  );

  deliveries.reduce((extraRun, delivery) => {
    if (matchId_2016.indexOf(delivery['match_id']) > -1) {
      if (extraRuns[delivery['bowling_team']] === undefined) {
        extraRuns[delivery['bowling_team']] = parseInt(delivery.extra_runs);
      }
      else {
        extraRuns[delivery['bowling_team']] += parseInt(delivery.extra_runs);
      }
    }
  });
  return extraRuns;
}

//End
//-------------------------------------------------------------------------------------

/**
 * Top 10 economical bowlers in the year 2015.
 * @param {*} matches: matches.csv data in json format 
 * @param {*} deliveries: deliveries.csv data in json format 
 */

function top10EconomicalBowlers_2015(matches, deliveries) {
  const economicalBowler_2015 = {};

  let matchId_2015 = matches.filter((match) => match.season == 2015).map(
    (map) => map.id
  );

  deliveries.reduce((bowler, delivery) => {
    if (matchId_2015.indexOf(delivery.match_id) > -1) {
      if (economicalBowler_2015[delivery['bowler']] === undefined) {
        economicalBowler_2015[delivery['bowler']] = {};
        economicalBowler_2015[delivery['bowler']]['balls'] = 1;
        economicalBowler_2015[delivery['bowler']]['runs'] = parseInt(delivery.total_runs);
      }
      else {
        economicalBowler_2015[delivery['bowler']]['runs'] += parseInt(delivery.total_runs);
        economicalBowler_2015[delivery['bowler']]['balls']++;
        economicalBowler_2015[delivery['bowler']]['economy'] = (economicalBowler_2015[delivery['bowler']]['runs'] / (economicalBowler_2015[delivery['bowler']]['balls'] / 6)).toFixed(2);
      }
    }
  });

  const economyOfBowlers = [];
  for (let bowlers in economicalBowler_2015) {
    economyOfBowlers.push(economicalBowler_2015[bowlers].economy);
  }
  economyOfBowlers.sort((a, b) => {
    return a - b;
  });

  const topTenBowlers = {};

  for (let index = 0; index <= 10; index++) {
    for (let key in economicalBowler_2015) {
      if (economicalBowler_2015[key].economy === economyOfBowlers[index]) {
        topTenBowlers[key] = economicalBowler_2015[key];
      }
    }
  }
  return topTenBowlers;
}

//End
//---------------------------------------------------------------------------------------------------------------------------------

module.exports = {
  matchesPerYear,
  matchesWonPerTeam,
  extraRunsConcededPerTeam_2016,
  top10EconomicalBowlers_2015,
};
